%global app                     mediawiki
%global d_bin                   %{_bindir}

Name:                           meta-mediawiki
Version:                        1.0.0
Release:                        1%{?dist}
Summary:                        META-package for install and upgrade MediaWiki
License:                        GPLv3

Source10:                       app.%{app}.backup.sh
Source11:                       app.%{app}.install.sh
Source12:                       app.%{app}.upgrade.sh

Requires:                       meta-system

%description
META-package for install and upgrade MediaWiki.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0755 %{SOURCE10} \
  %{buildroot}%{d_bin}/app.%{app}.backup.sh
%{__install} -Dp -m 0755 %{SOURCE11} \
  %{buildroot}%{d_bin}/app.%{app}.install.sh
%{__install} -Dp -m 0755 %{SOURCE12} \
  %{buildroot}%{d_bin}/app.%{app}.upgrade.sh


%files
%{d_bin}/app.%{app}.backup.sh
%{d_bin}/app.%{app}.install.sh
%{d_bin}/app.%{app}.upgrade.sh


%changelog
* Mon Jul 29 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-1
- Initial build.
